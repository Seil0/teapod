Dies ist der dritte beta Release von Teapod 1.0.0 mit Unterstützung für Cunchyroll.

* Diverse UI/UX Verbesserungen
* Playhead Updates werden nun alle 30 Sekunden durchgeführt
* Fehlende Playhead Updates beim schließen des Players behoben (#62)
* Abo Status und Stufe zum Accountscreen hinzugefügt
* Das Verhalten des "Nächste Episode" Buttons wurde verbessert (#53)

Alle Änderungen https://git.mosad.xyz/Seil0/teapod/compare/1.0.0-beta2...1.0.0-beta3
