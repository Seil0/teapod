# Teapod

Teapod is a unofficial App for Crunchyroll. It allows you to watch all your favourite animes from Crunchyroll on your android device. To use Teapod you need to have a account at Crunchyroll.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="75">](https://f-droid.org/de/packages/org.mosad.teapod/)

## Features
* Watch all animes from Crunchyroll on your Android device
* Native Player based on ExoPayer
* Prefer the OmU version via the app settings

## Screenshots
[<img src="https://www.mosad.xyz/images/Teapod/Teapod_Home.webp" width=180>](https://www.mosad.xyz/images/Teapod/Teapod_Home.webp)
[<img src="https://www.mosad.xyz/images/Teapod/Teapod_Library.webp" width=180>](https://www.mosad.xyz/images/Teapod/Teapod_Library.webp)
[<img src="https://www.mosad.xyz/images/Teapod/Teapod_Media.webp" width=180>](https://www.mosad.xyz/images/Teapod/Teapod_Media.webp)
[<img src="https://www.mosad.xyz/images/Teapod/Teapod_Search.webp" width=180>](https://www.mosad.xyz/images/Teapod/Teapod_Search.webp)

### License
Teapod is licensed under the terms and conditions of GPL 3. This Project is not associated with Crunchyroll in any way.

### Contributing
Currently you need to have an Crunchyroll account to contribute to Teapod. Contributing without one is impossible.If you want to contribute to Teapod and need an account on this gitea instance, please write an email.

### [FAQ](https://git.mosad.xyz/Seil0/teapod/wiki#hilfe)

#### Why is it called Teapod?
Teapod is a Acronym for "The ultimate anime app on demand", hence this project is called Teapod and not Teapot.

Teapod © 2020-2023 [@Seil0](https://git.mosad.xyz/Seil0)
