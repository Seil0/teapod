package org.mosad.teapod.util

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.commit
import org.mosad.teapod.R
import kotlin.system.exitProcess

/**
 * Show a fragment on top of the current fragment.
 * The current fragment is replaced and the new one is added
 * to the back stack.
 */
fun FragmentActivity.showFragment(fragment: Fragment) {
    supportFragmentManager.commit {
        replace(R.id.nav_host_fragment, fragment, fragment.javaClass.simpleName)
        addToBackStack(fragment.javaClass.name)
        show(fragment)
    }
}

/**
 * hide the status and navigation bar
 */
fun Activity.hideBars() {
    hideBars(window, window.decorView.rootView)
}

fun Activity.isInPiPMode(): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        isInPictureInPictureMode
    } else {
        false // pip mode not supported
    }
}

/**
 * Bring up launcher task to front
 */
fun Activity.navToLauncherTask() {
    val activityManager = (getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
    activityManager.appTasks.forEach { task ->
        val baseIntent = task.taskInfo.baseIntent
        val categories = baseIntent.categories
        if (categories != null && categories.contains(Intent.CATEGORY_LAUNCHER)) {
            task.moveToFront()
            return
        }
    }
}

/**
 * exit and remove the app from tasks
 */
fun Activity.exitAndRemoveTask() {
    finishAndRemoveTask()
    exitProcess(0)
}
