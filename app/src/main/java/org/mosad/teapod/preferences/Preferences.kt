package org.mosad.teapod.preferences

import android.content.Context
import android.content.SharedPreferences
import org.mosad.teapod.R
import org.mosad.teapod.util.DataTypes
import java.util.*

object Preferences {

    var preferredAudioLocale: Locale = Locale.forLanguageTag("en-US")
        internal set
    var preferredSubtitleLocale: Locale = Locale.forLanguageTag("en-US")
        internal set
    var autoplay = true
        internal set
    var devSettings = false
        internal set
    var theme = DataTypes.Theme.SYSTEM
        internal set

    // dev settings
    var updatePlayhead = true
        internal set

    private fun getSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )
    }

    fun savePreferredAudioLocal(context: Context, preferredLocale: Locale) {
        with(getSharedPref(context).edit()) {
            putString(context.getString(R.string.save_key_preferred_local), preferredLocale.toLanguageTag())
            apply()
        }

        this.preferredAudioLocale = preferredLocale
    }

    fun savePreferredSubtitleLocal(context: Context, preferredLocale: Locale) {
        with(getSharedPref(context).edit()) {
            putString(context.getString(R.string.save_key_preferred_local), preferredLocale.toLanguageTag())
            apply()
        }

        this.preferredSubtitleLocale = preferredLocale
    }

    fun saveAutoplay(context: Context, autoplay: Boolean) {
        with(getSharedPref(context).edit()) {
            putBoolean(context.getString(R.string.save_key_autoplay), autoplay)
            apply()
        }

        this.autoplay = autoplay
    }

    fun saveDevSettings(context: Context, devSettings: Boolean) {
        with(getSharedPref(context).edit()) {
            putBoolean(context.getString(R.string.save_key_dev_settings), devSettings)
            apply()
        }

        this.devSettings = devSettings
    }

    fun saveTheme(context: Context, theme: DataTypes.Theme) {
        with(getSharedPref(context).edit()) {
            putString(context.getString(R.string.save_key_theme), theme.toString())
            apply()
        }

        this.theme = theme
    }

    fun saveUpdatePlayhead(context: Context, updatePlayhead: Boolean) {
        with(getSharedPref(context).edit()) {
            putBoolean(context.getString(R.string.save_key_update_playhead), updatePlayhead)
            apply()
        }

        this.updatePlayhead = updatePlayhead
    }

    /**
     * initially load the stored values
     */
    fun load(context: Context) {
        val sharedPref = getSharedPref(context)

        preferredAudioLocale = Locale.forLanguageTag(
            sharedPref.getString(
                context.getString(R.string.save_key_preferred_audio_local), "en-US"
            ) ?: "en-US"
        )
        preferredSubtitleLocale = Locale.forLanguageTag(
            sharedPref.getString(
                context.getString(R.string.save_key_preferred_local), "en-US"
            ) ?: "en-US"
        )
        autoplay = sharedPref.getBoolean(
            context.getString(R.string.save_key_autoplay), true
        )
        devSettings = sharedPref.getBoolean(
            context.getString(R.string.save_key_dev_settings), false
        )
        theme = DataTypes.Theme.valueOf(
            sharedPref.getString(
                context.getString(R.string.save_key_theme), DataTypes.Theme.SYSTEM.toString()
            ) ?:  DataTypes.Theme.SYSTEM.toString()
        )

        // dev settings
        updatePlayhead = sharedPref.getBoolean(
            context.getString(R.string.save_key_update_playhead), true
        )
    }


}