package org.mosad.teapod.ui.activity.main.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.launch
import org.mosad.teapod.R
import org.mosad.teapod.databinding.FragmentMyListsBinding
import org.mosad.teapod.ui.activity.main.viewmodel.MyListsFragmentViewModel
import org.mosad.teapod.util.toItemMediaList

class MyListsFragment : Fragment() {

    private lateinit var binding: FragmentMyListsBinding
    private lateinit var pagerAdapter: FragmentStateAdapter

    private val model: MyListsFragmentViewModel by viewModels()

    private val fragments = arrayListOf<Fragment>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMyListsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // tab layout and pager
        pagerAdapter = ScreenSlidePagerAdapter(this)
        binding.pagerMyLists.adapter = pagerAdapter

        TabLayoutMediator(binding.tabMyLists, binding.pagerMyLists) { tab, position ->
            tab.text = when(position) {
                0 -> getString(R.string.my_list)
                1 -> getString(R.string.crunchylists)
                2 -> getString(R.string.downloads)
                else -> ""
            }
        }.attach()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                model.onUiState(viewLifecycleOwner.lifecycleScope) { uiState ->
                    when (uiState) {
                        is MyListsFragmentViewModel.UiState.Normal -> bindUiStateNormal(uiState)
                        is MyListsFragmentViewModel.UiState.Loading -> bindUiStateLoading()
                        is MyListsFragmentViewModel.UiState.Error -> bindUiStateError(uiState)
                    }
                }
            }
        }
    }

    private fun bindUiStateNormal(uiState: MyListsFragmentViewModel.UiState.Normal) {
        MediaFragmentSimilar(uiState.watchlistItems.toItemMediaList()).also {
            fragments.add(it)
            pagerAdapter.notifyItemInserted(fragments.indexOf(it))
        }
    }

    private fun bindUiStateLoading() {
        // currently not used
    }

    private fun bindUiStateError(uiState: MyListsFragmentViewModel.UiState.Error) {
        // currently not used
        Log.e(javaClass.name, "A error occurred while loading a UiState: ${uiState.message}")
    }

    /**
     * A simple pager adapter
     * TODO also present in MediaFragment
     */
    private inner class ScreenSlidePagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
        override fun getItemCount(): Int = fragments.size

        override fun createFragment(position: Int): Fragment = fragments[position]
    }

}