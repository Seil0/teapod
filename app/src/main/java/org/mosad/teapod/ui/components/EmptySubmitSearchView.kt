package org.mosad.teapod.ui.components

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.widget.TextView
import androidx.appcompat.R
import androidx.appcompat.widget.SearchView

// see https://stackoverflow.com/questions/30046201/android-searchview-empty-query-doesnt-work
class EmptySubmitSearchView : SearchView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setOnQueryTextListener(listener: OnQueryTextListener?) {
        super.setOnQueryTextListener(listener)

        findViewById<SearchAutoComplete?>(R.id.search_src_text).setOnEditorActionListener { _: TextView?, _: Int, event: KeyEvent? ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                listener?.onQueryTextSubmit(query.toString())
            } else {
                listener?.onQueryTextSubmit(query.toString())
            }

            false
        }
    }

}