/**
 * Teapod
 *
 * Copyright 2020-2022  <seil0@mosad.xyz>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

package org.mosad.teapod.ui.activity.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.mosad.teapod.databinding.FragmentMediaSimilarBinding
import org.mosad.teapod.util.ItemMedia
import org.mosad.teapod.util.adapter.MediaItemListAdapter
import org.mosad.teapod.util.showFragment

class MediaFragmentSimilar(val items: List<ItemMedia>) : Fragment()  {

    private lateinit var binding: FragmentMediaSimilarBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMediaSimilarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerMediaSimilar.adapter = MediaItemListAdapter(
            MediaItemListAdapter.OnClickListener {
                activity?.showFragment(MediaFragment(it.id))
            }
        )

        val adapterSimilar = binding.recyclerMediaSimilar.adapter as MediaItemListAdapter
        adapterSimilar.submitList(items)
    }
}