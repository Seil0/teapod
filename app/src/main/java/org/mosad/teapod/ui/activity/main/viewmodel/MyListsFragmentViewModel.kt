package org.mosad.teapod.ui.activity.main.viewmodel

import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.mosad.teapod.parser.crunchyroll.Crunchyroll
import org.mosad.teapod.parser.crunchyroll.Item

class MyListsFragmentViewModel : ViewModel() {

    private val WATCHLIST_LENGTH = 50

    private val uiState = MutableStateFlow<UiState>(UiState.Loading)

    sealed class UiState {
        object Loading : UiState()
        data class Normal(
            val watchlistItems: List<Item>
        ) : UiState()
        data class Error(val message: String?) : UiState()
    }

    init {
        load()
    }

    fun onUiState(scope: LifecycleCoroutineScope, collector: (UiState) -> Unit) {
        scope.launch { uiState.collect { collector(it) } }
    }

    fun load() {
        viewModelScope.launch {
            uiState.emit(UiState.Loading)
            try {
                // run the loading in parallel to speed up the process
                val watchlistJob = viewModelScope.async { Crunchyroll.watchlist(WATCHLIST_LENGTH).data }
                uiState.emit(
                    UiState.Normal(watchlistJob.await())
                )
            } catch (e: Exception) {
                uiState.emit(UiState.Error(e.message))
            }
        }

    }

}