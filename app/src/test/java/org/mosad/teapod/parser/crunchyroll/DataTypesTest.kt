package org.mosad.teapod.parser.crunchyroll

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.junit.Assert
import org.junit.Test

class DataTypesTest {

    @Test
    fun testTokenType() {
        val testToken = javaClass.getResource("/token.json")!!.readText()
        val token: Token = Json.decodeFromString(testToken)

        Assert.assertEquals("TestAccessToken-1_TestAccessToken", token.accessToken)
        Assert.assertEquals("00000000-0000-0000-0000-000000000000", token.refreshToken)
        Assert.assertEquals(300, token.expiresIn)
        Assert.assertEquals("Bearer", token.tokenType)
        Assert.assertEquals("account content offline_access reviews talkbox", token.scope)
        Assert.assertEquals("DE", token.country)
        Assert.assertEquals("00000000-0000-0000-0000-000000000000", token.accountId)
    }

}